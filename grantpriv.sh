#!/bin/sh

user=diaspora
dbname=diaspora_production

echo "Make $user user owner of $dbname database..."
sudo -u postgres psql -c "ALTER DATABASE $dbname OWNER to $user;" || {
  exit 1 
  }

echo "Allow $user user to create databases..."
sudo -u postgres psql -c "ALTER USER $user CREATEDB;" || {
  exit 1 
  }
echo "Grant all privileges to $user user..."
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE template1 to $user;" || {
  exit 1
  }


